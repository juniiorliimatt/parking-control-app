import axios from 'axios';
import { useEffect, useState } from 'react';

const api = 'http://localhost:8080/parking-spot';

export const App = () => {
  const [spots, setSpot] = useState<ParkingSpot[]>([]);
  let data: ParkingSpot[];

  const handleGetSpots = () => {
    axios.get(`${api}/find/all`).then((response) => {
      setSpot(response.data.data.content);
    });
  };

  useEffect(() => {
    handleGetSpots();
  }, []);

  const handleList = () => {
    {spots.map((spot: ParkingSpot) => {
          <li>{spot.apartment}</li>;
        }}
    );
  };

  return <ul>{handleList}</ul>;
};
