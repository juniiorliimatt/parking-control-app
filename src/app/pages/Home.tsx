import { useEffect, useState } from 'react';
import axios from 'axios';

export const Home = () => {
  const [list, setList] = useState<string[]>(['teste 1', 'teste 2', 'teste 3']);

  const handleGetList = () => {
    axios
      .get('http://localhost:8080/parking-spot/find/all')
      .then((response) => console.log(response));
  };

  useEffect(() => {
    handleGetList();
  });

  return (
    <ul>
      {list.map((value, index) => {
        <li key={index}>{value}</li>;
      })}
    </ul>
  );
};
