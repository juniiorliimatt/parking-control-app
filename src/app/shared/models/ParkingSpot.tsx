interface ParkingSpot {
  id: string;
  parkingSpotNumber: string;
  licensePlateCar: string;
  brandCar: string;
  modelCar: string;
  colorCar: string;
  registrationDate: string;
  responsibleName: string;
  apartment: string;
  block: string;
}
